import fs from "fs";
import express, { NextFunction, Request, Response } from "express";
import cors from "cors";
import morgan from "morgan";

const app = express();
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());

app.post("/", async (req: Request, res: Response, next: NextFunction) => {
    const writeStream = fs.createWriteStream(`./${req.headers["file_name"]}`, {
        encoding: "utf-8",
        flags: "a",
    });

    req.pipe(writeStream);
    req.on("end", () => {
        res.status(200).json("file saved.");
    });
});

app.listen(8899, () => {
    console.log("connnected to storage server");
});